// 220a first-order ambisonic encode for omnitone decode
// chuck -s --srate:48000 quadSlates2foa.ck

"" => string directory;
"" => string basename;
0::second => dur fileLength;
/* 
   directory, basename and fileLength must be set before running
   otherwise the script exits
   "/Users/kermit/Desktop/" => string basename; -- must end with '/'
   "test-" => string basename;
   10::second => dur fileLength;
*/
if((directory=="")||(basename=="")||(fileLength==0::samp))
{
  <<<"directory, basename and fileLength must be set before running, directory =",directory,"basename =",basename," fileLength =",fileLength/second>>>;
  me.exit();
}

4 => int nChans;

///////
// test with spoken slates

SndBuf ch[nChans];

for (0=>int i; i<nChans; i++) 
{
  ch[i].read(me.dir()+"slate-"+i+".wav");      // individual slates
  //ch[i] => dac.chan(i); // connect direct to a dac channel of 4-ch soundcard
}

///////////////////////////////////////////////////
class foa{
  4 => int nFOAchans;
  Gain out[nFOAchans]; 	// w,x,y,z
  4 => int nSrcs;
  Gain src[];	 	// empty until init is called
  float theta[];	// source angles
  Step srcOut[][];	// each source's FOA signals

  fun void init (float srcAngles[])	// initialize encoder with all source angles
  {
    srcAngles.cap() => nSrcs;
    srcAngles @=> theta;
    Gain tmp1[nSrcs] @=> src;	 	
    Step tmp2[nSrcs][nFOAchans] @=> srcOut;
    for (0 => int i; i<nSrcs; i++) spork ~encode(i);
  }

  fun void encode(int i)
  {
    src[i] => blackhole;					// signal in
    for (0 => int j; j<nFOAchans; j++) srcOut[i][j] => out[j];	// mix to foa out
    Math.sin(theta[i]) => float xcoeff;
    Math.cos(theta[i]) => float ycoeff;
    while(true)
    {
      srcOut[i][0].next( src[i].last() );		// w
      srcOut[i][1].next( src[i].last() * xcoeff );	// x
      srcOut[i][2].next( src[i].last() * ycoeff );	// y
      srcOut[i][3].next( 0.0 );				// z
      1::samp => now;
    }
  }

}

///////////////////////////////////////////////////
// list all incoming source angles we want to encode
3.14159265359 / 4.0 => float piOverFour;
[piOverFour, 3.0*piOverFour, 5.0*piOverFour, -piOverFour] @=> float thetas[];
foa encoder;
encoder.init(thetas);
for (0 => int i; i<nChans; i++) ch[i] => encoder.src[i];

WvOut w[4];
["w","x","y","z"] @=> string ambiName[]; // omnitone script will convert to ACN
for (0 => int i; i<4; i++) w[i].wavFilename(directory+basename+ambiName[i]+".wav");
for (0 => int i; i<encoder.nFOAchans; i++) encoder.out[i] => w[i] => blackhole;
for (0 => int i; i<4; i++) <<<"encoding to",(directory+basename+ambiName[i]+".wav")>>>;

now + fileLength => time quit;
<<<"for",fileLength/second,"seconds">>>;

// stall the main thread until we reach quit time 
while (now<quit) 1::samp => now;
<<<"done writing",nChans,"sources into 4 mono ambisonic component files">>>;

for (0 => int i; i<4; i++) w[i].closeFile();

// last steps when this is done, 
// cd to <directory>
// be certain that there are only the 4 recent basename(d) files when you execute
// ls <basename>*
// then, execute 
// sox -M <basename>* -c4 ambi.wav
// copy ambi.wav to the directory where omnitone will serve it from
// usually ~/Library/Web/220a/
// rename ambi.wav to the unique name referred to in your .html -- for example, hw2ambi.wav --
