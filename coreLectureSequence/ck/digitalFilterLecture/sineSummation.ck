// demo of sine summation and cancellation

Gain sum => dac.chan(0);
sum => WvOut w => blackhole;
w.wavFilename("/tmp/tmp.wav");

1::second => now; // second of silence at the start

SinOsc sin1 => sum;
sin1.freq(1000.0);
sin1.gain(0.5);

24::samp => now; // 24 = 1/2 of 1000Hz period

SinOsc sin2 => sum;
sin2.freq(1000.0);
sin2.gain(0.5);

2::second => now;

w.closeFile();
