FileIO fout;
fout.open( "score_mp.txt", FileIO.WRITE );
if( !fout.good() ) cherr <= "can't open file for writing..." <= IO.newline();

30.0 => float len;
fout <= "duration " <= len <= "\n";

class Osc
{
  SinOsc o => Gain g => blackhole;
  Step unity => g;
  g.gain(0.5);
}
Osc osc[4];
osc[0].o.freq(0.15); // x
osc[1].o.freq(0.155); // dur
osc[2].o.freq(0.16); // y
osc[3].o.freq(0.165); // max

string plate;
1 => int i;
now + len::second => time end;
0.001::second => now;

while (now<end)
{
Std.rand2(0,1) => int scrape;
now => time endStroke;
if(scrape) 0.5::second +=> endStroke;
while (now<=endStroke)
{
  now/second => float beg;
  osc[0].g.last() => float x;
  0.25 *=> x;
  0.7 +=> x;

  osc[2].g.last() => float y;
  0.25 *=> y;
  0.7 +=> y;

  osc[1].g.last() => float dur;
if(scrape)
{
  0.00005 *=> dur;
  0.0001 +=> dur;
} else {
  0.00001 *=> dur;
  0.00014 +=> dur;
}

Std.rand2(0,1) => int loud;
0.005 => float max; // small hit
Std.dbtorms(Std.rand2(106,130)) *=> max; 

  2000.0 *=> max;

  if (i%2) "plat1" => plate; else "plat2" => plate;
  fout <= "strike " <= beg <= " " <= plate <= " " <= x <= " "  <= y <= " "  <= dur <= " "  <= max <= "\n";
  dur*10::second => now;
}
Std.rand2f(0.9,1.1)::second => now;
}

fout.close();
