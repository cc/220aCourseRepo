["RF", "LF", "LR", "RR" ] @=> string channelNames[];

for (0=>int i; i<channelNames.cap(); i++) {
  channelNames[i] => string channelName;
  "/usr/ccrma/web/html/courses/220a-fall-2017/coreLectureSequence/coreLectureSequence/jukebox/quadExamples/turenas4ch/turenas" => string partialName;
  partialName + channelName + ".wav" => string fileName;
  <<<fileName>>>;
  SndBuf tmp;
  tmp.read(fileName);
  tmp => dac.chan(i);
  tmp.pos(48000*40); // to advance the position further into the file -- 40 secs
}

1::day=>now;

