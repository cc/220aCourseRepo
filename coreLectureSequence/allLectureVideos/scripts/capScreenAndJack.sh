#!/bin/bash
# http://wiki.linuxaudio.org/wiki/screencasttutorial

# for connected webcams do
# v4l2-ctl --list-devices
#1280x720 camera
#1600x900 desired
DATE=`date +%Y%m%d`
TIME=`date +%Hh%M`

# Start screencast
xterm -e jack_capture $HOME/Videos/$1.stereo.wav &
ffmpeg -an -f x11grab -r 30 -s 1600x900 -i :0.0 -vcodec libx264  -threads 4 -y $HOME/Videos/$1.screen.mkv

killall -sINT jack_capture

