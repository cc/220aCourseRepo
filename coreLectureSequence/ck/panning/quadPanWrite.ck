// quadPanning of impulse train, writes 4 mono .wav files into /tmp directory
// set to record for 10 seconds while also playing to dac
// see quadPan.ck for version that's dac only (doesn't write)
// dac output is i%2 so it's 4 channels mixed to 2

// declare an array of WvOut ugens
// one for each channel
4 => int nChans;
WvOut w[nChans];
// set basename for files
"beepChannel-" => string basename;

// set how long to record for
now + 10::second => time quit;

// position of speakers: LF, RF, LR, RR (Z-config)
[[-1.0, 1.0], [1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]] @=> float spks[][];

// position of source
float x,y;

// UGens
Gain in;
Gain out[4];

// initialization
for (0 => int i; i < nChans; ++i) {
  in => out[i] => dac.chan(i%2);
  // blackhole is a "sample sucker" to "pull" samples in the graph
  // here it is used to activate the WvOut's
  out[i] => w[i] => blackhole; // connect it to a WvOut
  basename + i => string tmp; // unique name for each file
  <<<"opening", tmp>>>; // print the name
  // full filename will need file directory and extension
  w[i].wavFilename("/tmp/" + tmp + ".wav"); // set full filename 
}

// setPosition(): implements simple DBAP. the radius
// of sound is set to 2.0 by default.
fun void setPosition(float x, float y) {
  x => x;
  y => y;
  for(0 => int i; i < 4; ++i) {
    spks[i][0] - x => float dx;
    spks[i][1] - y => float dy;
    dx * dx => dx;
    dy * dy => dy;
    Math.sqrt(dx + dy) => float dist;
    Math.max(0.0, 2.0 - dist) => dist;
    dist => out[i].gain;
  }
}

// setGain(): set overall gain
fun void setGain(float gain) {
gain => in.gain;
}
/////////////////////////////
// source sound
Impulse imp => in;

fun void snd() {
  while(true) {
    imp.next(1.0);
    100::ms => now;
  }
}
spork ~snd();

/////////////////////////////
// panning motion

0.0 => float t;
while (now < quit) {
  0.015 +=> t;
  // the two values represent the x and y coordinates to which we pan
  setPosition(Math.sin(t), 0);
  10::ms => now;
}

for (0=>int i; i<nChans; i++) 
{
    basename + i => string tmp;
    <<<"closing", tmp>>>;
    w[i].closeFile("/tmp/" + tmp + ".wav");
}
