/* 2018
  define a "Clip" class with state which can be queried and changed
*/

class Clip
{
  dur myDur, myIOI;
  float myStrength, myPitch;
  int myChannel;
  string name;
  false => int isPlaying;			// initial state
  fun void play()
  {
    StifKarp myString => dac.chan(myChannel); // wire up
    now => time myBeg;
    myBeg + myDur => time myEnd;
    while (now < myEnd)
    {
      myString.pluck(Math.dbtorms(100.0 + myStrength));
      myString.freq(Std.mtof(myPitch));
      myIOI => now;
    }
    false => isPlaying;				// state when finished
    myString =< dac.chan(myChannel); // unwire
  }
  fun void go(dur dur, dur ioi, float db, float kn, int ch) // set and play
  { 
    dur => myDur;
    ioi => myIOI;
    db => myStrength;
    kn => myPitch;
    ch => myChannel;
    if(!isPlaying) spork ~play(); 		// start independent shred
    true => isPlaying;				// state when started
  };
}

3 => int nClips;
Clip clip[nClips];				// new instances 
for (0=>int i; i<nClips; i++) "clip"+i => clip[i].name;

// returns true if any clip is playing
fun int join() 
{ 
  false => int tmp;
  for (0=>int i; (i<nClips && !tmp); i++) clip[i].isPlaying +=> tmp;
  return tmp;
};

for (0=>int i; i<3; i++)
{
  for (0=>int i; i<nClips; i++) clip[i].go((1+i*0.5)::second, 250::ms, -20.0, 56.0 + (i*2), (i%2));
  while (join()) 1::samp => now; // wait for all instances to stop playing
}
