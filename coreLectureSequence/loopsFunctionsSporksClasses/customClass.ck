dac => WvOut2 w => blackhole;
w.wavFilename("/tmp/tmp.wav");

class Bleep 
{
  Clarinet instrument;
  0 => int state;
  fun void nextNote ()
  {
    instrument.freq (Std.mtof( 60 + (state * 2) ));
    state++;
  }
}
Bleep bloop;

fun void play ()
{
  for (0 => int i; i < 5; i++)
  {
    i%2 => int ch;
    bloop.instrument => dac.chan(ch);
    bloop.nextNote();
    bloop.instrument.noteOn(0.6);
    500::ms => now;
    bloop.instrument.noteOff(0.001);
    50::ms => now;
  }
}

play();
