dac => WvOut2 w => blackhole;
w.wavFilename("/tmp/tmp.wav");

Clarinet a;
StifKarp b;
Bowed c;
Saxofony d;
BlowBotl e;
[ a, b, c, d, e ] @=> StkInstrument instrument[];

fun void play ()
{
  for (0 => int i; i < 5; i++)
  {
    i%2 => int ch;
    instrument[i] => dac.chan(ch);
    instrument[i].noteOn(0.6);
    500::ms => now;
    instrument[i].noteOff(0.001);
    50::ms => now;
    instrument[i] =< dac.chan(ch);
  }
}

play();
