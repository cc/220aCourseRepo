// string or tube from choice of low-pass loop filter

Impulse imp => DelayL d => dac;
Gain fb;	
//OnePole lpf;  // tube
OneZero lpf;    // string
fb => d => lpf => fb;
lpf.gain(-1.0); // octave
2.0::ms => d.delay;

dac => WvOut w => blackhole;
w.wavFilename("/tmp/tmp.wav");
now => time start;
start + 1::second => time end;

while (now<end)
{
  imp.next(1.0);
  0.5::second => now;
}
