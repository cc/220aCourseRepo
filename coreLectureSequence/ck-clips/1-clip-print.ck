/* 2018
  skeleton example which demonstrates "clip" concept 
  its clips don't play any sounds themselves but just print when 
  they fire and report how long they exist

  the "parent" program starts a sequence of "sporked" child clips at time t0
  and ends when it has nothing left to do
*/

// parent starts a sequence of clips by sporking them at particular times
now => time t0;

spork ~childClip1(5::second, t0); // global time = 0 sec
1::second => now;
spork ~childClip2(5::second, t0); // global time = 1 sec
5::second => now;
spork ~childClip1(1::second, t0); // global time = 6 sec
1::second => now;

now => time t1; // global time = 7 sec

// parent program will end with this print statement
<<<"parent lasted for",(t1-t0)/second,"seconds">>>;
// and with nothing left to do this program exits 

//////////////////////////////////////////////////
// here are the function definitions, they can appear later in the document

// define a "clip" as a function
fun void childClip1(dur myDur, time t0) {
  <<<"a childClip1 starting at",(now-t0)/second,"is playing for",myDur/second,"seconds">>>;
  myDur => now;
  <<<"a childClip1 stopped at time",(now-t0)/second>>>;
}

// define another "clip"
fun void childClip2(dur myDur, time t0) {
  <<<"a childClip2 starting at",(now-t0)/second,"is playing for",myDur/second,"seconds">>>;
  myDur => now;
  <<<"a childClip2 stopped at time",(now-t0)/second>>>;
}

