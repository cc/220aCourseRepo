declare name 		"oscByHand";
declare version 	"1.0";
declare author 		"220a2016";

//-----------------------------------------------
// 			Basic Sinusoidal Oscillator
//-----------------------------------------------
import("stdfaust.lib"); // ma.SR, ma.PI

db2linear(x)		= pow(10.0, x/20.0);
smooth(x)		= *(1-x) : +~*(x);
frac(x) 		= x - floor(x);

vol 			= hslider("volume", -50, -96, 0, 0.1) : smooth(0.999); 	// dB
freq 			= hslider("freq", 1000, 0, 2000, .01);			// Hz

mySinOsc(freq, vol) = amp * sin(phaseRad) 	// use built-in sin function
with
{
  amp= db2linear(vol);
  phaseInc= freq/float(ma.SR); 		// phase increment, depends on freq
  curPhase= (+(phaseInc) : frac)  ~ _;	// current phase, aka phasor signal
  phaseRad = 2.0*ma.PI*curPhase;	// phase in radians
};

// process 		= mySinOsc(440.0,-50.0);

process 		= mySinOsc(freq,vol);

