FileIO fout;
fout.open( "out.txt", FileIO.WRITE );
if( !fout.good() ) cherr <= "can't open file for writing..." <= IO.newline();

/*
# name of the plate, the X position,
# the Y position, the duration, and the maximum force. 
# The position values are normalised to the range 0-1.
*/

16.0 => float len;
fout <= "duration " <= len <= "\n";

SinOsc osc => Gain g => blackhole;
osc.freq(0.5);
Step unity => g;
g.gain(0.5);

0.350 => float y;
0.001 => float dur;
2000.0 => float max;

string plate;
0 => int i;
now + len::second => time end;
0.001::second => now;
while (now<end)
{
now/second => float beg;
g.last() => float x;
0.6 *=> x;
0.2 +=> x;
if (i%2) "plat1" => plate; else "plat2" => plate;
  fout <= "strike " <= beg <= " " <= plate <= " " <= x <= " "  <= y <= " "  <= dur <= " "  <= max <= "\n";
0.2::second => now;
i++;
}

fout.close();
