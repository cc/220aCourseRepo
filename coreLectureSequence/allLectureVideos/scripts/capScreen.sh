#!/bin/bash
# http://wiki.linuxaudio.org/wiki/screencasttutorial

# for connected webcams do
# v4l2-ctl --list-devices
#1280x720 camera
#1600x900 desired
DATE=`date +%Y%m%d`
TIME=`date +%Hh%M`

# turn off auto exposure for logitech webcam
# v4l2-ctl -d /dev/video1 --set-ctrl exposure_auto=1

# Start screencast
ffmpeg -an -f x11grab -r 30 -s 1920x1080 -i :0.0 -vcodec libx264  -threads 4 -y $HOME/Videos/$1.screen.mkv 
