// quadPanning of impulse train
// derived from Hongchan Choi's DBAP4.ck
// DBAP (distance-based amplitude panning)

// position of speakers: LF, RF, LR, RR (Z-config)
[[-1.0, 1.0], [1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]] @=> float spks[][];

// position of source
float x,y;

// UGens
Gain in;
Gain out[4];

// initialization
for (0 => int i; i < 4; ++i) {
  in => out[i] => dac.chan(i%2);
}

// setPosition(): implements simple DBAP. the radius
// of sound is set to 2.0 by default.
fun void setPosition(float x, float y) {
  x => x;
  y => y;
  for(0 => int i; i < 4; ++i) {
    spks[i][0] - x => float dx;
    spks[i][1] - y => float dy;
    dx * dx => dx;
    dy * dy => dy;
    Math.sqrt(dx + dy) => float dist;
    Math.max(0.0, 2.0 - dist) => dist;
    dist => out[i].gain;
  }
}

// setGain(): set overall gain
fun void setGain(float gain) {
gain => in.gain;
}
/////////////////////////////
// source sound
Impulse imp => in;

fun void snd() {
  while(true) {
    imp.next(1.0);
    100::ms => now;
  }
}
spork ~snd();

/////////////////////////////
// panning motion
0.0 => float t;
while(true) {
  0.015 +=> t;
  // the two values represent the x and y coordinates to which we pan
  setPosition(Math.sin(t), 0);
  10::ms => now;
}
