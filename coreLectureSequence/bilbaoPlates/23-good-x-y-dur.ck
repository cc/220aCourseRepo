FileIO fout;
fout.open( "out.txt", FileIO.WRITE );
if( !fout.good() ) cherr <= "can't open file for writing..." <= IO.newline();

16.0 => float len;
fout <= "duration " <= len <= "\n";

class Osc
{
  SinOsc o => Gain g => blackhole;
  Step unity => g;
  g.gain(0.5);
}
Osc osc[3];
osc[0].o.freq(0.5); // x
osc[1].o.freq(0.55); // dur
osc[2].o.freq(0.6); // y

2000.0 => float max;

string plate;
0 => int i;
now + len::second => time end;
0.001::second => now;
while (now<end)
{
  now/second => float beg;
  osc[0].g.last() => float x;
  0.6 *=> x;
  0.2 +=> x;

  osc[1].g.last() => float dur;
  0.1 *=> dur;
  0.001 +=> dur;

  osc[2].g.last() => float y;
  0.6 *=> y;
  0.2 +=> y;

  if (i%2) "plat1" => plate; else "plat2" => plate;
  fout <= "strike " <= beg <= " " <= plate <= " " <= x <= " "  <= y <= " "  <= dur <= " "  <= max <= "\n";
  0.2::second => now;
  i++;
}

fout.close();
