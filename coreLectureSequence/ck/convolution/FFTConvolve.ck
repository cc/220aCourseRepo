// chuck -s --chugin:/usr/local/lib/chuck/FIR.chug FIRConvolveStereo.ck 

// brute force convolution in time domain


// FIRConvolveStereo.ck 2017 -- two hall convolution
// .wav input is mono dry orchestra, denon9Mono48.wav
// convolved with musikvereinIR.wav to left channel
// convolved with unknownHallIR.wav to right channel
// set irDir to where soundfiles are e.g., coreLectureSequence/ir-convolution
"../../ir-convolution/" => string irDir;
// FFT convolution with static impulse response
// by Perry R. Cook, November 2014
// upsides:  as efficient as it could be, save for 
//           constructing a specific fft convolution chugin
// downsides: minimum delay is length of impulse response + buffers
// fix:      break into pieces and overlap add
//  Other fix:  see filter version using my FIR Filter chugin

// our fixed convolution kernal (impulse response)
SndBuf s => FFT ffth => blackhole;  
irDir + "musikvereinIR.wav" => s.read; // whatever you like (caution of length!!)
2 => int fftSize;
while (fftSize < s.samples()) 
    2 *=> fftSize;           // next highest power of two
fftSize => int windowSize;   // this is windowsize, only apply to signal blocks
windowSize/2 => int hopSize; // this can any whole fraction of windowsize
2 *=> fftSize;               // zero pad by 2x factor (for convolve)
// our input signal, replace adc with anything you like
SndBuf s2 => Gain input => FFT fftx => blackhole;  // input signal
irDir + "denon9Mono48.wav" => s2.read;
true => s2.loop;
IFFT outy => dac;            // our output
fftSize => ffth.size => fftx.size => outy.size; // sizes
Windowing.hann(windowSize) => fftx.window;
   <<< s.samples(), fftSize >>>;
windowSize::samp => now;     // load impulse response into h
ffth.upchuck()  @=> UAnaBlob H; // spectrum of fixed impulse response
s =< ffth =< blackhole;      // don't need impulse resp signal anymore

complex Z[fftSize/2];
30000 => input.gain;          // fiddle with this how you like/need
WvOut w;
false => int rec;
fun void record(time startRec)  {
  while (true)
  {
    if (!rec && (now>startRec)) {
      dac => WvOut w => blackhole;
      w.wavFilename("/tmp/tmp.wav");
      true => rec;
    }
    1::samp => now;
  }
} spork ~record(now+(2*fftSize)::samp);

now + 2::second +(2*fftSize)::samp => time later;
while(now < later) {
    fftx.upchuck()  @=> UAnaBlob X; // spectrum of input signal

    // multiply spectra bin by bin (complex for free!):
    for(0 => int i; i < fftSize/2; i++ ) {
        fftx.cval(i) * H.cval(i) => Z[i];	
    }    
    outy.transform( Z );      // take ifft
    hopSize :: samp => now;   // and do it all again
}
w.closeFile();

