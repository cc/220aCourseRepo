declare name 		"fmfs";
declare version 	"1.0";
declare author 		"Grame";
declare license 	"BSD";
declare copyright 	"(c)GRAME 2009";

//-----------------------------------------------
// 			FM From Scratch
//-----------------------------------------------

import("stdfaust.lib");

amp 			= hslider("volume", 0.5, 0.0, 1.0, 0.01) : si.smoo ;
cFreq 			= hslider("cFreq [unit:Hz]", 1000, 20, 10000, 1) : si.smoo ;
index 			= hslider("index", 1.0, 0.0, 22.0, 0.01) : si.smoo ;
mRatio 			= hslider("mRatio", 1.0, 0.0, 100.0, 0.001)  *0.01: si.smoo ;

modGain = cFreq * index;
modFreq = cFreq * mRatio;
mod = os.osc(modFreq) * modGain;
out = os.osc(cFreq + mod) * amp;

process 		= out;

