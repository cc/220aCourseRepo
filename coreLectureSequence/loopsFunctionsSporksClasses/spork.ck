dac => WvOut2 w => blackhole;
w.wavFilename("/tmp/tmp.wav");

fun void playClar ()
{
  Clarinet clar => dac.chan(0);
  for (0 => int i; i < 5; i++)
  {
    clar.stopBlowing(0.001);
    50::ms => now;
    clar.startBlowing(0.6);
    500::ms => now;
  }
}

fun void playKarp ()
{
  StifKarp karp => dac.chan(1);
  for (0 => int i; i < 5; i++)
  {
    50::ms => now;
    karp.pluck(0.6);
    500::ms => now;
  }
}

spork ~playClar();
playKarp();
