# https://ffmpeg.org/pipermail/ffmpeg-user/2013-June/015662.html
# ffmpeg -i 0.mkv -i 0.wav -c:v copy -c:a aac -strict experimental output.mp4

ffmpeg -i $HOME/Videos/$1.screen.mkv -i $HOME/Videos/$1.face.mkv -filter_complex "[0:v:0]pad=iw*2:ih[bg]; [bg][1:v:0]overlay=w*2.5" -i $HOME/Videos/$1.stereo.wav  -c:a aac -strict experimental -y $HOME/Videos/$1.mp4
