// same as dacBeeps.ck
// only it has a fixed duration and will 
// write out a mono .wav file for each
// channel

// always "stop" and "start" virtual machine before writing out
// soundfiles from miniAudicle

// plays through dac.chan(i%2) for stereo soundcard use

// to just generate the files as fast as possible don't use miniAudicle
// use chuck direct in a terminal e.g., for 8 channels:
// chuck -s --channels:8 --srate:48000 dacBeepsWrite.ck 

8 => int nChans;

// declare an array of oscillators
SinOsc ch[nChans];

// set how long to record for
now + 10::second => time quit;

// declare an array of WvOut ugens
// one for each channel
WvOut w[nChans];
// set basename for files
"beepChannel-" => string basename;

// for each channel
for (0=>int i; i<nChans; i++) 
{
    new SinOsc @=> ch[i]; // instantiate an oscillator
    ch[i].gain(0.0);      // mute it
    ch[i] => dac.chan(i%2); // connect it to a dac channel
    // blackhole is a "sample sucker" to "pull" samples in the graph
    // here it is used to activate the WvOut's
    ch[i] => w[i] => blackhole; // connect it to a WvOut
    basename + i => string tmp; // unique name for each file
    <<<"opening", tmp>>>; // print the name
    // full filename will need file directory and extension
    w[i].wavFilename("/tmp/" + tmp + ".wav"); // set full filename 
}

0.2 => float amp; // overall level

// function to beep once for channel 0, twice for channel 1, etc.
fun void beep( int n )
{
   n-1 => int c;
   for (0=>int i; i<n; i++) 
   {
     ch[c].gain(amp); // unmute
     100::ms => now;  // stall
     ch[c].gain(0.0); // mute
     100::ms => now;  // stall
   }
   500::ms => now; // stall some more
   // "beep" will take 700ms for n=1, 900ms for n=2, etc.
}

// create an infinite pattern that rotates around the speakers
while (now < quit) for (1=>int n; n<=nChans; n++) beep(n);

for (0=>int i; i<nChans; i++) 
{
    basename + i => string tmp;
    <<<"closing", tmp>>>;
    w[i].closeFile("/tmp/" + tmp + ".wav");
    10::ms => now;  // stall for chuck and file system to finish
}
