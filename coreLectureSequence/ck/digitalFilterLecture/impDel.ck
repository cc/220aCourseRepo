// demo of writing impulse + delayed impulse
// writes .wav file useful for spectral analysis

Impulse imp => Gain sum => dac.chan(0);
sum => WvOut w => blackhole;
w.wavFilename("/tmp/tmp.wav");

4800::samp => dur delay;
if (delay > 0::ms) {
    <<<"adding a delayed branch">>>;
  imp => DelayL del => sum;
  del.max(delay);
  del.delay(delay);
}

1::second => now; // second of silence at the start
now + 2::second => time quit;

while (now < quit) {
  imp.next(0.5);
  500::ms => now;
}

w.closeFile();
