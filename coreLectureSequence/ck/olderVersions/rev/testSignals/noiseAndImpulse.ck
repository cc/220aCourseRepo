// all frequencies
 
"/zap/test.wav" => string writeName;

dac => WvOut o => blackhole;
writeName => o.wavFilename;

// noise to dac
Noise n => dac;

500::ms => now;

n.gain(0.0);

500::ms => now;

// impulse to dac
Impulse I => dac;

for( int i; i < 3; i++ )
    {
	I.next(1.0);
        500::ms => now;
    }

<<<"done">>>;
