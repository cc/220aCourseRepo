FileIO fout;
fout.open( "out.txt", FileIO.WRITE );
if( !fout.good() ) cherr <= "can't open file for writing..." <= IO.newline();

/*
# name of the plate, the X position,
# the Y position, the duration, and the maximum force. 
# The position values are normalised to the range 0-1.
*/

16.0 => float len;
fout <= "duration " <= len <= "\n";

0.001 => float beg;
0.001 => float x;
0.350 => float y;
0.007 => float dur;
1000.0 => float max;
for (0=>int i; i<5; i++)
{
  fout <= "strike " <= beg <= " " <= "plat1 " <= x <= " "  <= y <= " "  <= dur <= " "  <= max <= "\n";
1.5 +=> beg;
0.2 +=> x;
}

fout.close();
