// dyn-notes.ck
// demo of dynamical system melodies
// writes a sound file /zap/dyn.wav
// from miniAudicle, stop the vm to complete file output after playing the shred

Clarinet c => dac;

// how many phrases to play in the demo
5 => int numRiffs;

// use this polynomial function, with a0 term as the "heat" variable 
// y = a0 + a1x + a2x^2
fun float poly( float x, float r)
{
// set the other terms to constants
	return r * x * (1.0 - x);
}

// to write soundfile
dac => WvOut o => blackhole;
"/zap/dyn.wav" => o.wavFilename;

// return a linear interpolation proportional to x, between lo and hi
fun float interp( float lo, float hi, float x)
{
	return lo + (x * (hi - lo));
}

// return x, bounded by lo and hi
fun float clip( float lo, float hi, float x)
{
	return Math.min (Math.max (x , lo), hi);
}

// loop over the phrases
for( int ctr; ctr < numRiffs; ctr++ )
{
// calculate a ramp that increases from 0 to 1 at end of the phrases
	(((ctr)$float)/(numRiffs-1$float)) => float rampUp;
// use the ramp to increase the value of a0 in the polynomial above
	interp(3.4, 4.0, rampUp) => float heat;
    <<< "r `heat':", heat >>>;
// x is the iterated map state variable, use the same initial condition each new phrase
	0.1 => float v;
// play one phrase with melody based on iterated map
	for( int i; i < 40; i++ )               // loop for 40 notes
	{
		poly(v, heat) => v;                 // feedback x to iterate the map
        440.0 + (v * 220.0)  => float f;    // use the new value as a frequency
		clip (50.0, 4000.0, f) => f;        // bounded
	    f => c.freq;                        // to clarinet
		0.80 => c.startBlowing;             // note on
	    100::ms => now;                     // for 100ms
		1.0 => c.stopBlowing;               // then, note off
	}
	500::ms => now;                         // wait 500ms before beginning next phrase
}

// close soundfile
o.closeFile;
// in miniAudicle, you need to manually stop "virtual machine" to complete file output