SinOsc sin => dac;
now + 10::second => time later;
while (now < later)
{
    sin.gain(0.1);
    1::second => now;
    sin.gain(0.0);
    1::second => now;
}