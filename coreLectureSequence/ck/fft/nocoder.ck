//=INPUT=============================================================================
adc.left 
/*
SndBuf snd;
snd.read("/home/cc/Desktop/220a2015/ck/simulationNot.wav");
snd.loop(true);
snd
*/
=> PoleZero dcblock_mic => FFT fft_mic => blackhole; // input-mic signal
//=OUTPUT============================================================================
IFFT ifft_output => PoleZero dcblock_output => Gain output => dac; // output signal
dcblock_output.gain(0.1);

// unit-generator initial values
0.5 => output.gain;

// both of these one-pole one-zero filters remove "zero-frequency components"
// from the signal. This allows the output to be louder without distortion.
0.999 => dcblock_mic.blockZero;
0.999 => dcblock_output.blockZero;

// constant values for fft_mic, and ifft_output
512 => int FFT_SIZE => fft_mic.size => ifft_output.size; // display demo 256 = fast, 512 = slow
FFT_SIZE => int WIN_SIZE;
FFT_SIZE/32 => int HOP_SIZE;
32 => int printBins;
// generate a Hann window for use with fft_mic and ifft_output
Windowing.hann(WIN_SIZE) => fft_mic.window => ifft_output.window;

complex spectrum_mic[WIN_SIZE/2]; // spectrum array for mic transform

0 => int hopCtr;
1 => int zoomFreq; // for low end freq display, cuts the spectrum by this factor

//-VOCODER PROCESSING---------------------------------------------------------------
fun void vocode_filter() {
    while( true ) {
        fft_mic.upchuck(); // take mic fft
        fft_mic.spectrum(spectrum_mic); // retrieve results of mic transform
        if (!(hopCtr%FFT_SIZE)) // once every FFT
        {
          float avg[printBins];
          for( 0 => int i; i < avg.cap(); i++ ) 0.0 => avg[i];
          for( 0 => int i; i < (WIN_SIZE/2)/zoomFreq; i++ )
          {
            Math.sqrt(spectrum_mic[i].re*spectrum_mic[i].re + spectrum_mic[i].im*spectrum_mic[i].im) => float mag;
            mag +=> avg[ (i*zoomFreq) / ((WIN_SIZE/2)/avg.cap()) ];
          }
          for( 0 => int i; i < avg.cap(); i++ ) 100000.0 *=> avg[i];
          for( 0 => int i; i < printBins; i++ ) 
          { 
            "" => string line;
            (Math.rmstodb(avg[i])-80.0)$int => int db;
            if (db < 0) 0 => db;
            for( 0 => int j; j < db; j++ ) "." +=> line;
            <<<line,"*">>>; 
          }
          <<<"\n","\n">>>;
        }
        ifft_output.transform(spectrum_mic); // take inverse transform of our new altered synth transform
        HOP_SIZE::samp => now;
        hopCtr++;
    }
}
//----------------------------------------------------------------------------------

spork ~vocode_filter(); // run the vocoder
1::day => now;