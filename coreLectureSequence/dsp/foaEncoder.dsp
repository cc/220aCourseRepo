declare name "foaEncoder";
declare title "foaEncoder";

/*
based on example from
// http://kokkinizita.linuxaudio.org/linuxaudio/downloads/index.html
declare author "Pierre Guillot";
declare author "Eliott Paris";
declare author "Julien Colafrancesco";
declare copyright "2012-2013 Guillot, Paris, Colafrancesco, CICM labex art H2H, U. Paris 8";
*/

// import("hoa.lib");
 import("stdfaust.lib");
// import("math.lib");

az 	= hslider("azim [unit:Deg][style:knob]", 0, -180, 180, 1);
el 	= hslider("elev [unit:Deg][style:knob]", 0, -180, 180, 1);

encode(az,el,in) = w,x,y,z
with {
DEG2RAD = 0.01745329;
MIN3DB = 0.707107;
    azr = az * DEG2RAD;
    elr = el * DEG2RAD;
    zz = sin (elr);
    ce = cos (elr);
    xx = ce * cos (-azr);
    yy = ce * sin (-azr);

        w = MIN3DB * in;
        x = xx * in;
        y = yy * in;
        z = zz * in;
};
//test with noise
//process = no.noise : vgroup("encode", encode(az,el));
process = vgroup("encode", encode(az,el));
