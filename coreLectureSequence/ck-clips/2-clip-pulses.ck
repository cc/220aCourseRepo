/* 2018
  demonstrate "clip" concept that plays beeps
  one function is sporked twice, at different times, on different channels
*/
now => time t0;

spork ~childSawPulses(5::second, t0, 0); // global time = 0 sec
1.5::second => now;
spork ~childSawPulses(5::second, t0, 1); // global time = 1.5 sec
5::second => now;

now => time t1; // global time = 6.5 sec

<<<"parent lasted for",(t1-t0)/second,"seconds">>>;

//////////////////////////////////////////////////
// define a function that plays 1 second pulses to a given channel
fun void childSawPulses(dur myDur, time t0, int channel) {
  <<<"a childSawPulses clip starting at",(now-t0)/second,"is playing for",myDur/second,"seconds">>>;
  SawOsc mySin => Envelope env => dac.chan(channel); // wire up an oscillator and an envelope to the DAC
  now => time myBeg;
  myBeg + myDur => time myEnd;
  while (now < myEnd)
  {
    env.keyOn();
    100::ms => now;
    env.keyOff();
    900::ms => now;
  }
  <<<"a childSawPulses clip stopped at time",(now-t0)/second>>>;
}

