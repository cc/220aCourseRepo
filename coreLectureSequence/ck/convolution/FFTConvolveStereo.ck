// chuck -s FFTConvolveStereo.ck 

// efficient convolution in frequency domain

// FFTConvolveStereo.ck 2017 -- two hall convolution
// .wav input is mono dry orchestra, denon9Mono48.wav
// convolved with musikvereinIR.wav to left channel
// convolved with unknownHallIR.wav to right channel
// set irDir to where soundfiles are e.g., coreLectureSequence/ir-convolution
"../../ir-convolution/" => string irDir;

// this example extends
// FFTConvolve.ck  by Perry R. Cook, October 2012
// minimum delay is length of impulse response + buffers

// impulse response soundfiles
SndBuf irSig0 => FFT fft0 => blackhole; 
irDir + "musikvereinIR.wav" => irSig0.read;
SndBuf irSig1 => FFT fft1 => blackhole; 
irDir + "unknownHallIR.wav" => irSig1.read;

2 => int irSig0fftSize; 					// FFT sizes from lengths of IR's
while (irSig0fftSize < irSig0.samples()) 2 *=> irSig0fftSize;   // next highest power of two
2 => int irSig1fftSize;
while (irSig1fftSize < irSig1.samples()) 2 *=> irSig1fftSize;
irSig0fftSize => int windowSize;
if (irSig1fftSize > irSig0fftSize) irSig1fftSize => windowSize;
windowSize/2 => int hopSize; 					// any whole fraction of windowsize
windowSize*2 => int fftSize;               			// zero pad by 2x factor 

IFFT ir0 => Delay d0 => dac.chan(0);				// the patch
IFFT ir1 => Delay d1 => dac.chan(1);
fftSize => fft0.size => ir0.size; 				// FFT & IFFT sizes
fftSize => fft1.size => ir1.size;
// delay the convolved signals slightly after direct
0.03 :: second => d0.max => d0.delay; 
0.03 :: second => d1.max => d1.delay;
   <<< "IR sizes and FFT size:", irSig0.samples(), irSig1.samples(), fftSize >>>;
windowSize::samp => now;     					// load IR's into FFT's
fft0.upchuck()  @=> UAnaBlob H0; 				// spectrum of IR's
fft1.upchuck()  @=> UAnaBlob H1;
irSig0 =< fft0 =< blackhole;      				// don't need IR signals anymore
irSig1 =< fft1 =< blackhole;

complex Z0[fftSize/2];						// for output spectra
complex Z1[fftSize/2];

WvOut2 w;							// trigger .wav write after time delay
false => int rec;
fun void record(time startRec)  {
  while (true)
  {
    if (!rec && (now>startRec)) {
      dac => w => blackhole;
      w.wavFilename("/tmp/tmp.wav");
      true => rec;
    }
    1::samp => now;
  }
} spork ~record(now + 12::second);				// with this spork

SndBuf inputWav => FFT fftx => blackhole;  			// input signal
irDir + "denon9Mono48.wav" => inputWav.read;
// true => input.loop;						// if desired
fftSize => fftx.size; // sizes
Windowing.hann(windowSize) => fftx.window;			// hanning window
30000 => inputWav.gain;          				// compensate gain

now + 33::second => time later;
while(now < later) {
    fftx.upchuck()  @=> UAnaBlob X; 				// take spectrum of input signal

    // multiply spectra bin by bin (complex for free!):
    for(0 => int i; i < fftSize/2; i++ ) {
        fftx.cval(i) * H0.cval(i) => Z0[i];	
        fftx.cval(i) * H1.cval(i) => Z1[i];	
    }    
    ir0.transform( Z0 );      					// take ifft's for output
    ir1.transform( Z1 );      
    hopSize :: samp => now;   					// and do it all again
}
w.closeFile();

