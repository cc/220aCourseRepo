/* 2018
  demonstrate a new, custom class that plays beeps
  has internal state for setting pitch and inter-onset interval (IOI)
*/
now => time t0;

Saw saw;
saw.go(15::second, t0, 1); // global time = 0 sec
2::second => now;
saw.setPitch(60); // global time = 2 sec
2::second => now;
saw.setPitch(64); // global time = 4 sec
saw.setIOI(300::ms);

2::second => now;
now => time t1; // global time = 6 sec


<<<"parent lasted for",(t1-t0)/second,"seconds">>>;

//////////////////////////////////////////////////
// define a class with a function that plays 1 second pulses to a given channel
class Saw {
  SawOsc mySin;
  900::ms => dur IOI;
  fun void childSawPulses(dur myDur, time t0, int channel) {
    <<<"a childSawPulses clip starting at",(now-t0)/second,"is playing for",myDur/second,"seconds">>>;
    mySin => Envelope env => dac.chan(channel); // wire up an oscillator and an envelope to the DAC
    now => time myBeg;
    myBeg + myDur => time myEnd;
    while (now < myEnd)
    {
      env.keyOn();
      100::ms => now;
      env.keyOff();
      IOI => now;
    }
    <<<"a childSawPulses clip stopped at time",(now-t0)/second>>>;
  }
  fun void go(dur myDur, time t0, int channel) {
    spork ~childSawPulses(myDur, t0, channel);
  }
  fun void setPitch(int kn) {
    mySin.freq(Std.mtof(kn));
  }
  fun void setIOI(dur d) {
    d => IOI;
  }
}
