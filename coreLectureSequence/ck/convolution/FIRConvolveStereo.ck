// chuck -s --chugin:/usr/local/lib/chuck/FIR.chug FIRConvolveStereo.ck 

// brute force convolution in time domain

// FIRConvolveStereo.ck 2017 -- two hall convolution
// .wav input is mono dry orchestra, denon9Mono48.wav
// convolved with musikvereinIR.wav to left channel
// convolved with unknownHallIR.wav to right channel
// set irDir to where soundfiles are e.g., coreLectureSequence/ir-convolution
"../../ir-convolution/" => string irDir;

// demonstrates use of a chugin (a loadable object)
// this one is FIR

// this example extends
// FIRConvolve.ck  by Perry R. Cook, October 2012

// FIR chugin is an FIR filter to store the samples
// of an impulse response (or arbitrary soundfile).
// Not very efficient, (lots of CPU useage) but 
// shows how it works from the definition.

// the FIR chugin ships with chuck but won't run unless loaded
// its linux chuck installation path is /usr/local/lib/chuck/FIR.chug
// see command line at top
FIR ir0;					// left channel convolver
FIR ir1;					// right channel convolver

// impulse response soundfiles
SndBuf irSig0 => blackhole; 
irDir + "musikvereinIR.wav" => irSig0.read;
SndBuf irSig1 => blackhole; 
irDir + "unknownHallIR.wav" => irSig1.read;

irSig0.samples() => ir0.order => int maxOrder;	// how many IR samples to load
irSig1.samples() => ir1.order;
if (ir1.order() > ir0.order()) ir1.order() => maxOrder;
for (0 => int i; i < maxOrder; i++) {		// load 'em
    if(i < ir0.order()) ir0.coeff(i,irSig0.last());
    if(i < ir1.order()) ir1.coeff(i,irSig1.last());
    1.0 :: samp => now;
}

SndBuf inputWav => blackhole;  			// input signal
irDir + "denon9Mono48.wav" => inputWav.read;
// true => inputWav.loop;			// if desired

inputWav => ir0 => Delay d0 => dac.chan(0);	// the patch
inputWav => ir1 => Delay d1 => dac.chan(1);
inputWav.gain(0.05);				// compensate gain
// delay the convolved signals slightly after direct
0.03 :: second => d0.max => d0.delay; 
0.03 :: second => d1.max => d1.delay;

dac => WvOut2 w => blackhole;			// write out .wav file
w.wavFilename("/tmp/tmp.wav");

22.0 :: second => now;
w.closeFile();


