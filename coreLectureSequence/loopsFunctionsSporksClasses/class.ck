dac => WvOut2 w => blackhole;
w.wavFilename("/tmp/tmp.wav");

Clarinet clar;
StifKarp karp;
[ clar, karp ] @=> StkInstrument inst[];

fun void play (int ch)
{
  inst[ch] => dac.chan(ch);
  for (0 => int i; i < 5; i++)
  {
    inst[ch].noteOff(0.001);
    50::ms => now;
    inst[ch].noteOn(0.6);
    500::ms => now;
  }
}

spork ~play(0); // call function in a separate shred
play(1);
