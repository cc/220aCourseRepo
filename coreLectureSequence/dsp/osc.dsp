declare name 		"osc";
declare version 	"1.0";
declare author 		"Grame";
declare license 	"BSD";
declare copyright 	"(c)GRAME 2009";

//-----------------------------------------------
// 			Sinusoidal Oscillator
//-----------------------------------------------

// from os.osc.dsp
// modified to be amplitude, not db
// added tc and smooth freq

import("stdfaust.lib");

tc = 0.021;

vol 	= hslider("volume [unit:dB]", -20, -96, 0, 0.1) : si.smooth(ba.tau2pole(tc));
freq 	= hslider("freq [unit:Hz]", 1000, 0, 20000, 1) : si.smooth(ba.tau2pole(tc)) ;
process 		= vgroup("Oscillator", os.osc(freq) * vol);

