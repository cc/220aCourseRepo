// dyn-del-ins.ck
// demo of dynamical system tones, same as dyn-ins.ck
// but with delay and low-pass filter in feedback

class MapTick
{

  Gain x => Gain tmp => Gain poly1;
  x => Gain poly2;
  tmp => poly2;
  3 => poly1.op; 
  3 => poly2.op;
  Step a0 => Gain out;
  Step a1 => poly1 => out;
  Step a2 => poly2 => out;
		
  out => Envelope e => dac => DelayL d => OneZero lpf => Gain loop => x;

  fun void startNote( float c)
  {
    c => a0.next;
    -0.7 => a1.next;
    2.0 => a2.next;
  // set delay to constant, 500 Hz = 2 msec
    2.0::ms => d.delay;
    0.95 => loop.gain;
    e.keyOn();
    10::ms => dur t => e.duration;
  }

  fun void stopNote()
  {
    e.keyOff();
  }
}

// to write soundfile specify file name
string writeName;
"/tmp/dyn.wav" => writeName;
if (writeName!="")
{
	dac => WvOut o => blackhole;
	writeName => o.wavFilename;
}


MapTick ins;

20 => int numTones;

for( int ctr; ctr < numTones; ctr++ )
{
	(((ctr)$float)/(numTones-1$float)) => float ramp;
	-0.3 + (ramp * (-0.7 - -0.3)) => float a0;
    	<<< "a0:", a0 >>>;
	ins.startNote(a0);
	400::ms => now;
	ins.stopNote();
	100::ms => now;
}
