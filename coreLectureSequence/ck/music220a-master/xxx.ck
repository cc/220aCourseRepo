fun void xxx (float f)
{
  SinOsc tri => dac.chan(0);
  tri.gain(0.0);
  tri.freq(f); // 100 sample period
  for (0 => int i; i < 6; i++)
  {
    tri.gain(0.5);
    500::ms => now;
    tri.gain(0.0);
    500::ms => now;
  }
}
spork ~xxx(480.0);
50::samp => now;
spork ~xxx(482.0);

1::day => now;