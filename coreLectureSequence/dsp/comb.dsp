declare name 		"comb";
declare license 	"BSD";

/*
  various comb filters for 220a demonstration purposes
  needs an audio signal at the input to hear anything
  simpleComb is like my chuck demo
  the others are from faust/lib/filters.lib
  for standalone -- compile with
  faust2jaqt comb.dsp 
*/

import("stdfaust.lib");

maxdel			= pow(2,16);

//------------------------------ User Interface -----------------------------------

freq 			= hslider("freq ", 100, 50, 4000, 1);
damp			= hslider("damping ", 0.9, 0.3, 0.99999, 0.00001);

simpleComb(dt, damp) = (+:@(dt)) ~  *(damp);
//----------------------------------- Process -------------------------------------

//process 		= _;
process 		= vgroup("comb filter", simpleComb(ma.SR/freq,damp));
//process 		= vgroup("comb filter", fi.ffcombfilter(maxdel,ma.SR/freq,damp));
//process 		= vgroup("comb filter", fi.fb_fcomb(maxdel,ma.SR/freq,1,-damp));

