// reads mono file and processes with ResonZ while writing mono file

string filename0;
"simulationNot.wav" => filename0;
string filename1;
"/tmp/test.wav" => filename1;
<<< "processing mono sound file ",filename0," into ",filename1>>>;

SndBuf w0 => ResonZ r => WvOut w1 => dac;

r.freq(1300.0); // resonance freq
r.Q(300.0); // sharpness
r.gain(130.0); // might need boost

filename0 => w0.read;
filename1 => w1.wavFilename;

w0.length() => dur l;
now + l + 1::second => time then; // 1 second tail
while( now < then )
{
100::samp => now;
}

w1.closeFile();

