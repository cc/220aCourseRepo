declare name "tsarWeeded"; // early code derived from Romain Michon's STK Modal Bar
// from dsp16/faustLive.../srateBugWorkaround/

import("stdfaust.lib");

// Stick impact table is a synthesized strike rather than what was in instrument.h which was from a force hammer

marmstk1TableSize = 251;
readMarmstk1 = rdtable(strikeTable);
strikeTable = waveform{
0.00000, 	0.0107956, 	0.00906154, 	0.00730403, 	0.00560427, 	0.00403535, 	0.00266097, 	0.00153439, 
0.000697541, 	0.00018035, 	1.1917e-07, 	0.000161452, 	0.000655981, 	0.00146267, 	0.00254811, 	0.00386708, 
0.00536337, 	0.0069707, 	0.00861397, 	0.0102104, 	0.0116712, 	0.0129029, 	0.0138093, 	0.014293, 
0.0142573, 	0.0136082, 	0.0122562, 	0.0101177, 	0.00711765, 	0.00319047, 	-0.0017179, 	-0.00764862, 
-0.0146286, 	-0.0226692, 	-0.0317656, 	-0.0418953, 	-0.0530182, 	-0.0650762, 	-0.0779924, 	-0.0916722, 
-0.106003, 	-0.120855, 	-0.136082, 	-0.151523, 	-0.167003, 	-0.182336, 	-0.197322, 	-0.211757, 
-0.225429, 	-0.238121, 	-0.249615, 	-0.259696, 	-0.268151, 	-0.274772, 	-0.279363, 	-0.281737, 
-0.281724, 	-0.279167, 	-0.273931, 	-0.265903, 	-0.254991, 	-0.241129, 	-0.22428, 	-0.204434, 
-0.181611, 	-0.15586, 	-0.127265, 	-0.0959367, 	-0.0620203, 	-0.0256907, 	0.0128471, 	0.0533588, 
0.0955826,	0.139231, 	0.183992, 	0.229533, 	0.275501, 	0.321529, 	0.367235, 	0.412228, 
0.456112, 	0.498486, 	0.538952, 	0.577115, 	0.612592, 	0.645007, 	0.674005, 	0.699248, 
0.720423, 	0.737243, 	0.74945, 	0.75682, 	0.759165, 	0.756333, 	0.748213, 	0.734738, 
0.71588, 	0.691658, 	0.662136, 	0.62742, 	0.587665, 	0.543067, 	0.493866, 	0.440343, 
0.382819, 	0.321653, 	0.257239, 	0.190002, 	0.120396, 	0.0489004, 	-0.0239852, 	-0.0977431, 
-0.171843, 	-0.245746, 	-0.318911, 	-0.390793, 	-0.460858, 	-0.528577, 	-0.593438, 	-0.654948, 
-0.712636, 	-0.766058, 	-0.814801, 	-0.858489, 	-0.896781, 	-0.929378, 	-0.956027, 	-0.976516, 
-0.990687, 	-0.998425, 	-0.999669, 	-0.994409, 	-0.982684, 	-0.964583, 	-0.940249, 	-0.909867, 
-0.873676, 	-0.831953, 	-0.785022, 	-0.733245, 	-0.677021, 	-0.616779, 	-0.55298, 	-0.48611, 
-0.416674, 	-0.345194, 	-0.272205, 	-0.198249, 	-0.12387, 	-0.0496099, 	0.0239942, 	0.0964173, 
0.16715, 	0.235701, 	0.301603, 	0.364417, 	0.423732, 	0.479173, 	0.5304, 	0.577113, 
0.619051, 	0.655998, 	0.687781, 	0.71427, 	0.735382, 	0.751078, 	0.761365, 	0.766294, 
0.765955, 	0.760484, 	0.750054, 	0.734874, 	0.715189, 	0.691274, 	0.663434, 	0.631997, 
0.597315, 	0.559757, 	0.519705, 	0.477553, 	0.433701, 	0.388553, 	0.342509, 	0.295969, 
0.24932, 	0.202942, 	0.157196, 	0.11243, 	0.0689669, 	0.0271097, 	-0.012865, 	-0.0507072, 
-0.0861957, 	-0.119139, 	-0.149377, 	-0.176779, 	-0.201249, 	-0.22272, 	-0.241158, 	-0.256557, 
-0.268942, 	-0.278367, 	-0.284911, 	-0.288677, 	-0.289793, 	-0.288407, 	-0.284683, 	-0.278802, 
-0.270961, 	-0.261362, 	-0.250219, 	-0.23775, 	-0.224175, 	-0.209714, 	-0.194583, 	-0.178997, 
-0.163159, 	-0.147266, 	-0.131501, 	-0.116035, 	-0.101026, 	-0.0866146, 	-0.0729237, 	-0.0600602, 
-0.048112, 	-0.0371488, 	-0.027222, 	-0.0183643, 	-0.0105909, 	-0.00389995, 	0.00172687, 	0.00632284, 
0.00993513, 	0.0126233, 	0.0144579, 	0.0155186, 	0.0158924, 	0.0156721, 	0.0149541, 	0.0138367, 
0.0124184, 	0.0107956, 	0.00906154, 	0.00730403, 	0.00560427, 	0.00403535, 	0.00266097, 	0.00153439, 
0.000697541, 	0.00018035, 	1.1917e-07};

gain = 0.1;

uiGate = button("h:Attack/gate [1][tooltip:noteOn = 1, noteOff = 0]");

expVal = 0;

// older time constants
//0.999995, long carrilon before mar25
//0.99999, long tsar mar25
// 0.99998, // 5sec for dinnerbell
hiDamping = 0.9999780;

hiExc = 1.0;

loDamping = 0.9999990 ;

loExc = 1.0;

stickHardness = 0.469  ;

whackBoost = 0.9815 ;

// linear plus a touch of exp
cfSeries(i,x) = (0.75*x+1.0) * pow(loadCF(i), 1.0 + 0.001*x);

nPartials = 50;
loadCF = rdtable(cfTable);

loadX = rdtable(xTable);
///////////////////////////// insert here
import("faustPeaks.dat");

/////////////////////////////
loadY = rdtable(yTable);
yTable = xTable;

loadZ = rdtable(zTable);
zTable = xTable;

// lowest and highest partials as midi key nums
loKey = 31.31; 
hiKey = 92.31;
difKey = hiKey - loKey;
ftom(f) = (log(f/440.0) / LOGTWO) * 12.0 + 69
  with {
  LOGTWO = 0.69314718055994528623;
};

// damping depends on log freq
dampTilt(f) = xxx
  with {
    k = ftom(f);
    interp = 0.5; //  ( (k - loKey) / difKey );
    xxx = (interp*(hiDamping-loDamping))+loDamping;
};

// excitation amplitude depends on log freq
excTilt(f) = xxx
  with {
    k = ftom(f);
    interp = 0.5; // ( (k - loKey) / difKey );
    xxx = (interp*(hiExc-loExc))+loExc;
};

// bandPassH is declared in instrument.lib
bandPassH(resonance,radius) = fi.TF2(b0,b1,b2,a1,a2)
	with {
		a2 = radius*radius;
		a1 = -2*radius*cos(ma.PI*2*resonance/ma.SR);
		b0 = 1;
		b1 = 0;
		b2 = 0;
	};

skip = 0; // 10
keep = nPartials-skip; // 200

biquadBankX = _ <: sum(i, keep, oneFilter(i+skip)) 
  with{
    oneFilter(j,exc) =  cfSeries(j, expVal), dampTilt(cfSeries(j, expVal)), excTilt(cfSeries(j, expVal))*exc*(pow(loadX(j),whackBoost)): bandPassH; 
  };
biquadBankY = _ <: sum(i, keep, oneFilter(i+skip)) 
  with{
    oneFilter(j,exc) =  cfSeries(j, expVal), dampTilt(cfSeries(j, expVal)), excTilt(cfSeries(j, expVal))*exc*(pow(loadY(j),whackBoost)): bandPassH; 
  };
biquadBankZ = _ <: sum(i, keep, oneFilter(i+skip)) 
  with{
    oneFilter(j,exc) =  cfSeries(j, expVal), dampTilt(cfSeries(j, expVal)), excTilt(cfSeries(j, expVal))*exc*(pow(loadZ(j),whackBoost)): bandPassH; 
  };

//onePole is declared in instrument.lib
onePole(b0,a1,x) = (b0*x - a1*_)~_;

sourceFilter = onePole(b0,a1)
	with{
		b0 = 1 - 0.9;
		a1 = -0.9;
	};

excitation (sh) = counterSamples < (marmstk1TableSize*rate) : *(marmstk1Wave*(uiGate))
  with {
    dataRate(readRate) = readRate : (+ : ma.decimal) ~ _ : *(float(marmstk1TableSize));
//the reading rate of the stick table as a function of stickHardness
    rate = 0.25*pow(4,sh);
    counterSamples = (*(uiGate)+1)~_ : -(1);
    marmstk1Wave = readMarmstk1(int(dataRate(rate)*uiGate));
  };

//process = stickHardness : excitation : *(gain) <:biquadBankX+(0.5*biquadBankZ),biquadBankY+(0.5*biquadBankZ);

// with source filter
process = stickHardness : excitation : sourceFilter : *(gain) <:biquadBankX+(0.5*biquadBankZ),biquadBankY+(0.5*biquadBankZ);
