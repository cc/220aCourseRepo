// chuck --srate:48000 --chugin-path:../dsp/srateBugWorkaround/okToChug oscByHand.ck

oscByHand sinFaust => dac.chan(0);	// chugin from faust
SinOsc sinChuck => blackhole; // dac.chan(1);		// chuck's sine class

1500.0 => float freq;
-10.0 => float vol;
sinFaust.freq(freq);
sinFaust.volume(vol);

sinChuck.freq(sinFaust.freq());
sinChuck.gain(Std.dbtorms(100.0+sinFaust.volume()));

0.0 => float curPhase;
fun float mySinOsc(float freq, float vol)
{
  Std.dbtorms(100.0+vol) => float amp;
  freq/(second/samp) => float phaseInc; 	// phase increment, depends on freq
  phaseInc +=> curPhase;
  Math.floor(curPhase) -=> curPhase;		// current phase, aka phasor signal
  2.0*Math.PI*curPhase => float phaseRad;	// phase in radians
  return amp * Math.sin(phaseRad); 		    // use built-in sin function
}

Step val => dac.chan(1);	// constant to change each sample tick

// 1::day => now;
now + 2::second => time quit;
while (now<quit)
{
  val.next(mySinOsc(freq, vol));
  1::samp => now;
}
