/*
  define a single "clip" which is a sequence of impulse responses passed
through a filter
  adapted from listing6.10 from the ChucK book
  it prints when it begins and reports how long it will exist

  the program runs in real time starting the clip at TIME 0 and 
  ends when it has nothing left to do
      
  command line:
  chuck clip.ck

  you can imagine layering this with other filtered sounds.
  also, check out the example Listing6.15 that ships with miniAudicle!
File > Open Example > Book > digital artists > Chapter 6
*/

// the lowpass LPF, highpass HPF, and bandpass with resonance ResonZ

// define the "clip" as a function
fun void clip(dur myDur, FilterBasic filt)
{
    Impulse imp => filt => dac; // wire up an oscillator and an envelope to the DAC
  100 => filt.Q; // set filter Q (how sharp to make resonance) (turned up really high here!)
  now => time myBeg;
  <<<"\tclip start at",(now-myBeg)/second,"seconds">>>;
  myBeg + myDur => time myEnd;
  while (now < myEnd)
  {
    Math.random2f(500.0,2500.0) => filt.freq;
    // fire our impulse, and hang out a bit
    100.0 => imp.next;
    0.1 :: second => now;
  }
  <<<"\tclip end at",(now-myBeg)/second,"seconds">>>;
}


// start a sequence of clips
now => time time0;
clip(1::second, new LPF);
0.1::second => now;       
clip(1::second, new HPF);
0.5::second => now;       
clip(1::second, new BPF);
0.5::second => now;       
clip(1::second, new BRF);
0.5::second => now;       
clip(1::second, new ResonZ);
0.5::second => now;       
me.yield();             // on this exact sample, yield master shred so sporked one can finish first

now => time time1;

// last item in this program is this print statement
<<<"clips played for",(time1-time0)/second,"seconds">>>;
// and with nothing left to do this program exits 