dac => WvOut2 w => blackhole;
w.wavFilename("/tmp/tmp.wav");

class Bleep 
{
  Clarinet instrument;
  0 => int state;
  fun void nextNote (int ch, float amp, float detune)
  {
    instrument => dac.chan(ch);
    instrument.gain (amp);
    instrument.freq (detune + Std.mtof( 60 + (state * 2) ));
    instrument.noteOn(0.6);
    500::ms => now;
    instrument.noteOff(0.001);
    50::ms => now;
    state++;
  }
}

fun void play (float amp, float detune)
{
  Bleep bloop;
  for (0 => int i; i < 5; i++) bloop.nextNote(i%2, amp, detune);
}

for (0 => int i; i < 10; i++) 
{
  spork ~play(0.1, i*0.1);
  200::ms => now;
}
5::second => now;
