[220a home page](https://ccrma.stanford.edu/courses/220a-fall-2018/)

this repo contains lecture materials and example code

to hear the audio and view videos that are contained here point a browser at [coreLectureSequence](https://ccrma.stanford.edu/courses/220a-fall-2018/coreLectureSequence/)
****

this is a public project on
> https://cm-gitlab.stanford.edu/public

copy a read-only snapshot with the download button

or clone it to a repository on a local machine 
> git clone git@cm-gitlab.stanford.edu:cc/220aCourseRepo.git coreLectureSequence

(the repo's origin requires permission to be modified)


