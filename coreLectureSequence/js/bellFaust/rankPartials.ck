// chuck -s rankPartials.ck

// fft peaks from Snd
// in Snd listener eval 
// (peaks "/home/cc/Desktop/junesRing/bellPlayer/ck/sndPeaks.dat")
FileIO fin;
FileIO fout;
"sndPeaks.dat" => string finName;
"../dsp/faustPeaks.dat" => string foutName;
fin.open(finName,FileIO.READ);
fout.open(foutName,FileIO.WRITE);
0 => int ch;
<<<finName,fin.readLine()>>>;
<<<"",fin.readLine()>>>;
<<<"",fin.readLine()>>>;
<<<"",fin.readLine()>>>;
0 => int rows;
"go" => string str;
while(!(str=="")) { // fin.eof()) {
  fin.readLine() => str;
  rows++;
}
rows--;
<<<rows,"peaks on channel",ch>>>;
fin.open(finName,FileIO.READ);
  fin.readLine();
  fin.readLine();
  fin.readLine();
  fin.readLine();
float peaks[rows][2];
for (0 => int i; i< rows; i++) {
  fin => str;
  Std.atof(str) => peaks[i][0];
  fin => str;
  Std.atof(str) => peaks[i][1];
//  fin.readLine();
};

/*
// sort by min freq
int iMin; 
float dummy[2];
for ( 0 => int j; j < peaks.cap(); j++) { 
    //assume the min is the first item 
    j => iMin; 
    //test against elements after j to find the smallest 
    for ( j+1 => int i; i < peaks.cap() ; i++) { 
        if(peaks[i][0] < peaks[iMin][0]) { 
           i => iMin; 
        } 
    } 
    peaks[j] @=> dummy; 
    peaks[iMin] @=> peaks[j]; 
    dummy @=> peaks[iMin]; 
}
*/

// sort by max amp
int iMax; 
float dummy[2];
for ( 0 => int j; j < peaks.cap(); j++) { 
    //assume the max is the first item
    j => iMax; 
    //test against elements after j to find the largest 
    for ( j+1 => int i; i < peaks.cap() ; i++) { 
        if(peaks[i][1] > peaks[iMax][1]) { 
           i => iMax; 
        } 
    } 
    peaks[j] @=> dummy; 
    peaks[iMax] @=> peaks[j]; 
    dummy @=> peaks[iMax]; 
}

0 => int cnt;
0.0 => float dc;
0.0005 => float loAmp;
"cfTable = waveform{" => string outStr;
for(int k; k < peaks.cap(); k++) {
//<<< "cf [", k,"] = ", peaks[k][0] >>>;
  if((peaks[k][0]>dc)&&(peaks[k][1]>loAmp)) { // thresholds
    peaks[k][0] +=> outStr;
    if(k<peaks.cap()-1) ",\t" +=> outStr; else "};\n" +=> outStr;
    cnt++;
    if(!(cnt%8)) "\n" +=> outStr;
  }
}
"\n" +=> outStr;
//////////////////////
0 => cnt;
"xTable = waveform{" +=> outStr;
for(int k; k < peaks.cap(); k++) {
//<<< "cf [", k,"] = ", peaks[k][0] >>>;
  if((peaks[k][0]>dc)&&(peaks[k][1]>loAmp)) { // thresholds
    peaks[k][1] +=> outStr;
    if(k<peaks.cap()-1) ",\t" +=> outStr; else "};\n" +=> outStr;
    cnt++;
    if(!(cnt%8)) "\n" +=> outStr;
  }
}
fout <= outStr;
fout.close();
<<<"wrote",foutName>>>;


