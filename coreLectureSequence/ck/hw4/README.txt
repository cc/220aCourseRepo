Homework 4 should be a quick study to work with some remaining essentials. Its components might help you imagine related mini-project possibilities.

Set up a microphone for real-time input to chuck. This can be the one you built earlier or whatever is convenient to plug into the soundcard you're using.

Test each of the examples listed in the collection below in miniAudicle. 
They introduce several new concepts in turn:
adc input
UGen's for effects processing (resonator, pitch shifter)
custom pattern generating functions (chaos, periodic, random)
real-time control mechanisms (computer keyboard)
FFT-based tracking (spectral centroid, RMS amplitude)

Homework 4's assignment is to use adc input in combination with effects processing and pattern generation.
Record a demo "performance" on some original code with those components. Turn in a recorded (stereo) .wav file and your final chuck code. (record with audacity or with WvOut2 -- in which case don't forget to close the output file appropriately)

Those are the essentials, but don't consider the assignment to be limited only to them. 
Feel free to (optionally) incorporate real-time control and/or FFT-based tracking. Also, any aspect of earlier assignment work. For more effects, control mechanisms and FFT-based tracking see the pertinent sections of included in the chuck examples.

The three final examples below are "combinations." Like with HW3, don't just recycle the examples provided. Customize and if you have time, layer multiple musical "voices" so there's more than one thing going on simultaneously. That can either be done in the same chuck script or by post-processing e.g., layering .wav files using audacity or merging them with sox.

inOut.ck
// hook a mic to speaker through chuck
(see also basic/adc.ck)

inEfx.ck
// resonance filter affecting mic signal with slow, rectified sine pattern changing the resonance
(see also filter/resonz.ck)

chaotic.ck / plotChaotic.ck
// print or plot values from the logistic map chaotic function
x is chaos state
r is chaos "heat" (control variable)
the program example runs independent of time 

plotPeriodic.ck
// plot low frequency periodic signal
f is wave frequency
a is wave amplitude
example depends on time

plotRandom.ck
// plot a random walk 
l is low bound
h is hi bound
example is independent of time 

FFT.ck
// mic or instrument input through trackers from analysis/centroid.ck and analysis/rms.ck
SinOsc sonifies tracker outputs
c is instantaneous centroid of input signal
r is RMS amplitude of input signal
(see also analysis/fft.ck)
(see also analysis/features/centroid.ck)
(see also analysis/features/rms.ck)

////////////////////////////////////////////////////////////////////////
combineEfxRandom.ck
// mic or instrument input through pitch shifter with random variation
PitShift effect, Random class from above
(see also book/digital-artists/chapter6/Listing6.14.ck)

combineClarinetChaotic.ck
// combine clarinet physical model with chaotic variation
Clarinet UGen, Logistic class from above

kbdControlChaotic.ck
// combine computer keyboard control with clarinet and chaotic variation
Hid Class, Clarinet UGen, Logistic class
(see also hid/kb.ck)

