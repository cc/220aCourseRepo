/* 2018
  use WvOut to write 4 mono files into /tmp

  can be faster than real time if run in command line:
  chuck -s --channels:4 6-longerDemo-write-4-mono-files.ck
*/

WvOut w[4]; // 4 WvOuts
for (0=>int i; i<4; i++) {
  dac.chan(i) => w[i] => blackhole;
  w[i].wavFilename("/tmp/quad" + i + ".wav");
}
// define the Clip class
class Clip
{ // class members and initial state
  dur myDur, myIOI;
  float myStrength, myPitch;
  int myChannel;
  -1 => int curChannel;
  string name;
  StifKarp myString;
  false => int isPlaying;
  // play a clip			
  fun void play()						// will run in in separate shred
  {
    true => isPlaying;						// keep track of state
    if (myChannel != curChannel) 				// channel change
    {
      if (curChannel >= 0) myString =< dac.chan(curChannel%4);	// unwire existing connection
      myString => dac.chan(myChannel%4);			// wire up
      myChannel => curChannel;					// remember
    }
    now => time myBeg;
    myBeg + myDur => time myEnd;
    while (now < myEnd)
    {
      myString.pluck(Math.dbtorms(100.0 + myStrength));		// myStrength is -100 to 0 dB
      myString.freq(Std.mtof(myPitch));				// myPitch is MIDI keynum
      myIOI => now;
    }
    false => isPlaying;						// state when finished
  }
  // shred launcher
  fun void go ()
  {
    if(!isPlaying) spork ~play();	// if there isn't one already, start separate shred
    me.yield();				// on this sample yield master shred so sporked ones will run
  }
  // set clip variables and go
  fun void setPlay(float msDur, float msIOI, float db, float kn, int ch)
  { 
    msDur::ms => myDur;
    msIOI::ms => myIOI;
    db => myStrength;
    kn => myPitch;
    ch => myChannel;
    go();
  };
  // offset clip variables and go
  fun void offsetPlay(float msDur, float msIOI, float db, float kn, int ch)
  { 
    msDur::ms +=> myDur;
    msIOI::ms +=> myIOI;
    db +=> myStrength;
    kn +=> myPitch;
    ch +=> myChannel;
    go();
  };
  // selectively reset any clip variables and go
  fun void resetPlay(float msDur, float msIOI, float db, float kn, int ch)
  { 
    if(msDur>0) msDur::ms => myDur;
    if(msIOI>0) msIOI::ms => myIOI;
    if(db<=0) db => myStrength;	// myStrength is -100 to 0, so ignore any positive integer
    if(kn>0) kn => myPitch;
    if(ch>0) ch => myChannel;
    go();
  };
}

5 => int nClips;
Clip clip[nClips];					// create new clip instances 
for (0=>int i; i<nClips; i++) "clip"+i => clip[i].name;	// and name them

fun void join() { while (clipAlive()) 1::samp => now; };
fun int clipAlive() 
{ 
  false => int tmp;
  for (0=>int i; (i<nClips && !tmp); i++) clip[i].isPlaying +=> tmp;
  return tmp;						// returns true if any clip is playing
};

for (0=>int i; i<2; i++)				// twice
{
  for (0=>int i; i<nClips; i++) 				// play layered clips
    clip[i].setPlay(1000+i*500, 250, -20.0, 56.0+i*2, i%4);	// setting all their parameters
  join();							// wait until all clips have finished
}

for (0=>int i; i<3; i++)				// three times
{
  for (0=>int i; i<nClips; i++) 				// play layered clips
    clip[i].offsetPlay(i*-100, -50, 2.0, 2, 1);			// decrementing/incrementing parameters
  join();							
}

for (0=>int i; i<nClips; i++) 				// extend the last texture
  clip[i].resetPlay(2000, 0, 1.0, 0, 0);			// changing only the clips' duration parameter
join();							

for (0=>int i; i<nClips; i++) 				// play tutti final note
  clip[i].offsetPlay(0, 3000, 5.0, 1, 2);			// incrementing IOI, dB, pitch
join();							

for (0=>int i; i<nClips; i++) 				// play tutti echo note
  clip[i].resetPlay(0, 2000, -25.0, 0, 0);			// reseting IOI, dB
join();					

for (0=>int i; i<4; i++) {
  w[i].closeFile();
  <<<"closed /tmp/quad" + i + ".wav">>>;
}
